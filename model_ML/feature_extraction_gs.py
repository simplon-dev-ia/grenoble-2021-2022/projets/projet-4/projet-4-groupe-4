import pandas as pd

from img_preprocessing import crop_img, resize_img, split_rgb, blurr_img, \
    rgb_hist, search_tresh, treshold, px_rate_rgb, gs, gs_hist, gs_treshold,\
    px_rate_gs, search_tresh_gs, blurr_gs


def feature_extract_gs(dataset):

    data = []
    i = 0
    limit = 500

    for pic_ref in dataset["name"]:
        df_loc_im = dataset[dataset["name"] == pic_ref]

        for index, df_pic_row in df_loc_im.iterrows():
            blurred_img_gs = blurr_gs(
                gs(
                    resize_img(
                        crop_img(df_pic_row)
                    )
                )
            )
            blurr_to_rate = px_rate_gs(
                gs_treshold(
                    search_tresh_gs(
                        gs_hist(blurred_img_gs)), blurred_img_gs
                )
            )
            data.append({"pic_ref": pic_ref,
                         "px_0": blurr_to_rate[0],
                         "px_1": blurr_to_rate[1],
                         "classname": df_pic_row["classname"]})

        i += 1
        print(f'processed {i}/{len(dataset)} img file')
        if i == limit:
            break

    df_data = pd.DataFrame(data).reset_index()

    return df_data
